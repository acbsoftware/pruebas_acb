package com.acbsoftware.tiempo.constant;

public class MeteoGaliciaConstant {

	public static final String URI_SERVIZOS = "http://servizos.meteogalicia.gal";

	public static final String URI_LISTADO_ESTACIONS = URI_SERVIZOS + "/rss/observacion/estadoEstacionsMeteo.action";
	public static final String URI_ICONOS_CEO = "https://www.meteogalicia.gal/datosred/infoweb/meteo/imagenes/meteoros/ceo/"; // .png
	public static final String URI_ICONOS_TEMP = "https://www.meteogalicia.gal/datosred/infoweb/meteo/imagenes/termometros/"; // .png
	public static final String URI_ICONOS_VENTO = "https://www.meteogalicia.gal/datosred/infoweb/meteo/imagenes/meteoros/vento/combo/"; // .png

	public static final String URL_PREDICION = URI_SERVIZOS + "/rss/predicion";
	public static final String URI_PREDICION_DIA = URL_PREDICION + "/rssCPrazo.action?request_locale=gl&dia=";
	public static final String URI_PREDICION_MAPA_MANANA = URL_PREDICION + "/cprazo/getImaxeM.action?dia=";
	public static final String URI_PREDICION_MAPA_TARDE = URL_PREDICION + "/cprazo/getImaxeT.action?dia=";
	public static final String URI_PREDICION_MAPA_NOITE = URL_PREDICION + "/cprazo/getImaxeN.action?dia=";
	public static final String URI_ICONOS_TENDENCIA = "https://www.meteogalicia.gal/datosred/infoweb/meteo/imagenes/termometros/"; // png

	public static final String URI_CORTES_VOZ = URI_SERVIZOS + "/rss/predicion/rssCortes.action";	
	public static final String URI_CAMARAS = URI_SERVIZOS + "/rss/observacion/jsonCamaras.action";
	
	public static final String ID_COMENTARIO_XERAL = "20";
	public static final String ID_COMENTARIO_ESPECIAL = "27";
	public static final String ID_TENDENCIA_TMAX = "30";
	public static final String ID_TENDENCIA_TMIN = "31";

	
}
