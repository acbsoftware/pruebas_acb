package com.acbsoftware.tiempo.service;

import com.acbsoftware.tiempo.model.IpInfo;

public interface IpInfoService {
	
	public abstract IpInfo getIPInfo();

}
