package com.acbsoftware.tiempo.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.acbsoftware.tiempo.constant.MeteoGaliciaConstant;
import com.acbsoftware.tiempo.model.meteogalicia.PredicionModel;
import com.acbsoftware.tiempo.model.meteogalicia.rss.RssParserItem;
import com.acbsoftware.tiempo.model.meteogalicia.rss.RssParserList;
import com.acbsoftware.tiempo.service.MeteoRssParserService;

public class MeteoRssParserServiceImpl implements MeteoRssParserService {

	@Override
	public RssParserList getRssPredicionXeral(URL url) {
		InputStream is = null;
		Document documento;
		try {
			is = url.openStream();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			documento = builder.parse(is);
			documento.getDocumentElement().normalize();

			Element item = (Element) documento.getElementsByTagName("item").item(0);

			List<RssParserItem> listItems = new ArrayList();
			RssParserItem rssItem = new RssParserItem();
			rssItem.setTitle(item.getElementsByTagName("title").item(0).getTextContent());
			rssItem.setLink(item.getElementsByTagName("link").item(0).getTextContent());
			rssItem.setDescription(item.getElementsByTagName("description").item(0).getTextContent());

			// CPrazo:variables
			Map<String, String> mapVariables = new HashMap();
			NodeList listVariables = item.getElementsByTagName("CPrazo:variables");
			for (int i = 0; i < listVariables.getLength(); i++) {
				Node itemVariables = listVariables.item(i);
				if (itemVariables.hasAttributes()) {
					String valor = itemVariables.getTextContent();
					String idItem = itemVariables.getAttributes().getNamedItem("id").getTextContent();
					if (idItem != null && valor != null) {
						mapVariables.put(idItem, valor);
					}
				}
			}
			rssItem.setcPrazoVariables(mapVariables);
			
			// CPrazo:temperaturas
			Map<String, Map<String, Byte>> mapTemperaturas = new HashMap();
			NodeList listTemperaturas = item.getElementsByTagName("CPrazo:temperaturas");
			for (int i = 0; i < listTemperaturas.getLength(); i++) {
				Node itemTemperatura = listTemperaturas.item(i);
				if (itemTemperatura.hasAttributes()) {
					String cidade=itemTemperatura.getTextContent();
					
					NamedNodeMap atributos = itemTemperatura.getAttributes();
					Map<String,Byte> temperaturas = new HashMap<>();
					temperaturas.put("tmax", Byte.parseByte(atributos.getNamedItem("tmax").getTextContent()));
					temperaturas.put("tmin", Byte.parseByte(atributos.getNamedItem("tmin").getTextContent()));
					
					mapTemperaturas.put(cidade, temperaturas);
				}
			}			
			rssItem.setcPrazoTemperaturas(mapTemperaturas);
			

			listItems.add(rssItem);

			RssParserList rssList = new RssParserList();
			rssList.setLink(documento.getElementsByTagName("link").item(0).getTextContent());
			rssList.setListItems(listItems);

			return rssList;
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public RssParserList getRssCortesVoz() {
		URL url;
		InputStream is = null;		
		Document documento;
		try {
			url = new URL(MeteoGaliciaConstant.URI_CORTES_VOZ);
			is = url.openStream();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			documento = builder.parse(is);
			documento.getDocumentElement().normalize();

			List<RssParserItem> listItems = new ArrayList();		

			// Collemos os 2 primeiros items do rss (Maña e Tarde)
			for (int j = 0; j < 2; j++) {
				Element item = (Element) documento.getElementsByTagName("item").item(j);			
				RssParserItem rssItem = new RssParserItem();
				rssItem.setTitle(item.getElementsByTagName("title").item(0).getTextContent());
				rssItem.setLink(item.getElementsByTagName("link").item(0).getTextContent());
				rssItem.setDcDate(item.getElementsByTagName("dc:date").item(0).getTextContent());			
				listItems.add(rssItem);
			}

			RssParserList rssList = new RssParserList();
			
			rssList.setListItems(listItems);

			return rssList;
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

}
