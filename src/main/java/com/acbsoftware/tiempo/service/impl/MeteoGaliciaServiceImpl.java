package com.acbsoftware.tiempo.service.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.acbsoftware.tiempo.constant.MeteoGaliciaConstant;
import com.acbsoftware.tiempo.model.meteogalicia.CamaraMeteoList;
import com.acbsoftware.tiempo.model.meteogalicia.ListaEstacionsList;
import com.acbsoftware.tiempo.model.meteogalicia.PredicionModel;
import com.acbsoftware.tiempo.model.meteogalicia.rss.RssParserItem;
import com.acbsoftware.tiempo.model.meteogalicia.rss.RssParserList;
import com.acbsoftware.tiempo.service.MeteoGaliciaService;
import com.acbsoftware.tiempo.service.MeteoRssParserService;

@Service("meteoGaliciaServiceImpl")
public class MeteoGaliciaServiceImpl implements MeteoGaliciaService {

	@Override
	public ListaEstacionsList getListadoEstacions() {
		final String uri = MeteoGaliciaConstant.URI_LISTADO_ESTACIONS;

		RestTemplate restTemplate = new RestTemplate();
		ListaEstacionsList listado = restTemplate.getForObject(uri, ListaEstacionsList.class);

		return listado;
	}

	@Override
	public PredicionModel getPredicion(int dia) {
		MeteoRssParserService meteoRssParser = new MeteoRssParserServiceImpl();
		try {
			RssParserList predicionRss = meteoRssParser.getRssPredicionXeral(new URL(MeteoGaliciaConstant.URI_PREDICION_DIA + dia));
			if (predicionRss != null) {
				PredicionModel model = new PredicionModel();
				RssParserItem item = predicionRss.getListItems().get(0);
				model.setTitle(item.getTitle());
				model.setPubDate(item.getPubDate());
				model.setComentarioXeral(item.getcPrazoVariables().get(MeteoGaliciaConstant.ID_COMENTARIO_XERAL));
				model.setComentarioEspecial(item.getcPrazoVariables().get(MeteoGaliciaConstant.ID_COMENTARIO_ESPECIAL));
				model.setTendenciaTMax(item.getcPrazoVariables().get(MeteoGaliciaConstant.ID_TENDENCIA_TMAX));
				model.setTendenciaTMin(item.getcPrazoVariables().get(MeteoGaliciaConstant.ID_TENDENCIA_TMIN));
				model.setTemperaturasCidades(item.getcPrazoTemperaturas());
				
				// Corte de voz
				Map<String, String> cortesVoz = new HashMap<>();
				for (RssParserItem meteoItem : meteoRssParser.getRssCortesVoz().getListItems()) {
					cortesVoz.put(meteoItem.getTitle(), meteoItem.getLink());					
				}
				model.setUrlsCortesVoz(cortesVoz);
				
				return model;
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
	
	@Override
	public CamaraMeteoList getListadoCamaras() {
		final String uri = MeteoGaliciaConstant.URI_CAMARAS;

		RestTemplate restTemplate = new RestTemplate();
		CamaraMeteoList listado = restTemplate.getForObject(uri, CamaraMeteoList.class);

		return listado;
	}

}
