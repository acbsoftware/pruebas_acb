package com.acbsoftware.tiempo.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.acbsoftware.tiempo.constant.IpInfoConstant;
import com.acbsoftware.tiempo.model.IpInfo;
import com.acbsoftware.tiempo.service.IpInfoService;

@Service("ipInfoServiceImpl")
public class IpInfoServiceImpl implements IpInfoService {

	@Override
	public IpInfo getIPInfo() {
		final String uri = IpInfoConstant.URI;

		RestTemplate restTemplate = new RestTemplate();
		IpInfo ipInfo = restTemplate.getForObject(uri, IpInfo.class);

		return ipInfo;
	}

}
