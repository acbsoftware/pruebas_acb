package com.acbsoftware.tiempo.service;

import java.net.URL;

import com.acbsoftware.tiempo.model.meteogalicia.rss.RssParserList;

public interface MeteoRssParserService {
	
	public abstract RssParserList getRssPredicionXeral(URL url);
	
	public abstract RssParserList getRssCortesVoz();
	
}
