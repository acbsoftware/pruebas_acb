package com.acbsoftware.tiempo.service;

import com.acbsoftware.tiempo.model.meteogalicia.CamaraMeteoList;
import com.acbsoftware.tiempo.model.meteogalicia.ListaEstacionsList;
import com.acbsoftware.tiempo.model.meteogalicia.PredicionModel;
import com.acbsoftware.tiempo.model.meteogalicia.rss.RssParserList;

public interface MeteoGaliciaService {
	
	public abstract ListaEstacionsList getListadoEstacions();
	
	public abstract PredicionModel getPredicion(int dia); // 0 = Hoxe; 1 = Maña		

	public abstract CamaraMeteoList getListadoCamaras();
	
}
