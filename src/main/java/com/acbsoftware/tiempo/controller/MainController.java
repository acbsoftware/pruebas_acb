package com.acbsoftware.tiempo.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.acbsoftware.tiempo.constant.ViewConstant;
import com.acbsoftware.tiempo.model.IpInfo;
import com.acbsoftware.tiempo.service.IpInfoService;

@Controller
public class MainController {

	private static final Log LOG = LogFactory.getLog(MainController.class);
	
	@Autowired
	@Qualifier("ipInfoServiceImpl")
	private IpInfoService ipInfoService;

	@GetMapping("/")
	public ModelAndView showMain() {
		ModelAndView mav = new ModelAndView(ViewConstant.MAIN);

		// Ip
		IpInfo ipInfo = ipInfoService.getIPInfo();
		if (null != ipInfo) {
			mav.addObject("ipinfo", ipInfo);
		}		
		
		return mav;
	}

}
