package com.acbsoftware.tiempo.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.acbsoftware.tiempo.constant.MeteoGaliciaConstant;
import com.acbsoftware.tiempo.constant.ViewConstant;
import com.acbsoftware.tiempo.model.meteogalicia.CamaraMeteoList;
import com.acbsoftware.tiempo.model.meteogalicia.ListaEstacionsList;
import com.acbsoftware.tiempo.model.meteogalicia.PredicionModel;
import com.acbsoftware.tiempo.service.MeteoGaliciaService;

@Controller
@RequestMapping("/meteogalicia")
public class MeteoGaliciaController {

	private static final Log LOG = LogFactory.getLog(MeteoGaliciaController.class);

	@Autowired
	@Qualifier("meteoGaliciaServiceImpl")
	private MeteoGaliciaService meteoGaliciaService;

	@GetMapping("/listaestacions")
	public ModelAndView showListaEstacions() {
		ModelAndView mav = new ModelAndView(ViewConstant.METEO_GALICIA);

		// Listado con todas las estaciones de Galicia
		ListaEstacionsList listadoEstacions = meteoGaliciaService.getListadoEstacions();

		if (null != listadoEstacions && listadoEstacions.getListEstadoActual().size() > 0) {
			mav.addObject("listadoEstacions", listadoEstacions.getListEstadoActual());
			mav.addObject("uriIconoCeo", MeteoGaliciaConstant.URI_ICONOS_CEO);
			mav.addObject("uriIconoTemp", MeteoGaliciaConstant.URI_ICONOS_TEMP);
			mav.addObject("uriIconoVento", MeteoGaliciaConstant.URI_ICONOS_VENTO);
			LOG.info("/listaestacions " + listadoEstacions.toString());
		}

		return mav;
	}

	@GetMapping("/predicionhoxe")
	public ModelAndView showPredicionHoxe() {
		ModelAndView mav = new ModelAndView(ViewConstant.METEO_GALICIA);
		LOG.info("/predicionhoxe ");

		PredicionModel model = meteoGaliciaService.getPredicion(0);

		if (null != model) {
			mav.addObject("model", model);
			mav.addObject("static", new MeteoGaliciaConstant());
		}

		return mav;
	}

	@GetMapping("/predicionmana")
	public ModelAndView showPredicionMana() {
		ModelAndView mav = new ModelAndView(ViewConstant.METEO_GALICIA);
		LOG.info("/predicionmana ");

		PredicionModel model = meteoGaliciaService.getPredicion(1);

		if (null != model) {
			mav.addObject("model", model);
			mav.addObject("static", new MeteoGaliciaConstant());
		}

		return mav;
	}

	@GetMapping("/camaras")
	public ModelAndView showCamarasMeteo() {
		ModelAndView mav = new ModelAndView(ViewConstant.METEO_GALICIA);

		// Listado con todas las camaras de Galicia
		CamaraMeteoList listadoCamaras = meteoGaliciaService.getListadoCamaras();

		if (listadoCamaras != null && listadoCamaras.getListaCamaras().size() > 0) {
			mav.addObject("listadoCamaras", listadoCamaras.getListaCamaras());
			mav.addObject("static", new MeteoGaliciaConstant());
			LOG.info("/predicionhoxe ");
		}

		return mav;
	}

}
