
package com.acbsoftware.tiempo.model.meteogalicia;

import java.util.HashMap;
import java.util.Map;

public class CamaraMeteo {

    private String concello;
    private Integer idConcello;
    private Integer identificador;
    private String imaxeCamara;
    private String imaxeCamaraMini;
    private Double lat;
    private Double lon;
    private String nomeCamara;
    private String provincia;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getConcello() {
        return concello;
    }

    public void setConcello(String concello) {
        this.concello = concello;
    }

    public Integer getIdConcello() {
        return idConcello;
    }

    public void setIdConcello(Integer idConcello) {
        this.idConcello = idConcello;
    }

    public Integer getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Integer identificador) {
        this.identificador = identificador;
    }

    public String getImaxeCamara() {
        return imaxeCamara;
    }

    public void setImaxeCamara(String imaxeCamara) {
        this.imaxeCamara = imaxeCamara;
    }

    public String getImaxeCamaraMini() {
        return imaxeCamaraMini;
    }

    public void setImaxeCamaraMini(String imaxeCamaraMini) {
        this.imaxeCamaraMini = imaxeCamaraMini;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getNomeCamara() {
        return nomeCamara;
    }

    public void setNomeCamara(String nomeCamara) {
        this.nomeCamara = nomeCamara;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
