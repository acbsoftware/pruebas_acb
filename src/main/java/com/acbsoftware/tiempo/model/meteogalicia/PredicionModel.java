package com.acbsoftware.tiempo.model.meteogalicia;

import java.util.Date;
import java.util.Map;

public class PredicionModel {

	private String title;
	private String descripcionHtml;
	private String pubDate;
	private String comentarioXeral; // <CPrazo:variables id="20" ...
	private String comentarioEspecial; // <CPrazo:variables id="27" ...
	private String tendenciaTMax; // <CPrazo:variables id="30" ...
	private String tendenciaTMin; // <CPrazo:variables id="31" ...
	private Map<String, Map<String, Byte>> temperaturasCidades; // String = nomeCidade; HashMap<tmax/tmin, temperatura>
	private Date dataPredicion; // formato "dd/MM/yyyy"
	private Map<String, String> urlsCortesVoz; // Galicia Mañá - Galicia Tarde
	
	public Map<String, String> getUrlsCortesVoz() {
		return urlsCortesVoz;
	}

	public void setUrlsCortesVoz(Map<String, String> urlsCortesVoz) {
		this.urlsCortesVoz = urlsCortesVoz;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescripcionHtml() {
		return descripcionHtml;
	}

	public void setDescripcionHtml(String descripcionHtml) {
		this.descripcionHtml = descripcionHtml;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	public String getComentarioXeral() {
		return comentarioXeral;
	}

	public void setComentarioXeral(String comentarioXeral) {
		this.comentarioXeral = comentarioXeral;
	}

	public String getComentarioEspecial() {
		return comentarioEspecial;
	}

	public void setComentarioEspecial(String comentarioEspecial) {
		this.comentarioEspecial = comentarioEspecial;
	}

	public String getTendenciaTMax() {
		return tendenciaTMax;
	}

	public void setTendenciaTMax(String tendenciaTMax) {
		this.tendenciaTMax = tendenciaTMax;
	}

	public String getTendenciaTMin() {
		return tendenciaTMin;
	}

	public void setTendenciaTMin(String tendenciaTMin) {
		this.tendenciaTMin = tendenciaTMin;
	}

	public Map<String, Map<String, Byte>> getTemperaturasCidades() {
		return temperaturasCidades;
	}

	public void setTemperaturasCidades(Map<String, Map<String, Byte>> temperaturasCidades) {
		this.temperaturasCidades = temperaturasCidades;
	}

	public Date getDataPredicion() {
		return dataPredicion;
	}

	public void setDataPredicion(Date dataPredicion) {
		this.dataPredicion = dataPredicion;
	}

}
