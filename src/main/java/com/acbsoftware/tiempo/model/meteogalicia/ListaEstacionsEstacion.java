package com.acbsoftware.tiempo.model.meteogalicia;

import java.util.HashMap;
import java.util.Map;

public class ListaEstacionsEstacion {

    private Integer altitude;
    private String concello;
    private String dataLocal;
    private String dataUTC;
    private String estacion;
    private Integer idEstacion;
    private Double lat;
    private Integer lnIconoCeo;
    private Integer lnIconoTemperatura;
    private Integer lnIconoVento;
    private Double lon;
    private String provincia;
    private String utmx;
    private String utmy;
    private Double valorSensTermica;
    private Double valorTemperatura;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Integer getAltitude() {
        return altitude;
    }

    public void setAltitude(Integer altitude) {
        this.altitude = altitude;
    }

    public String getConcello() {
        return concello;
    }

    public void setConcello(String concello) {
        this.concello = concello;
    }

    public String getDataLocal() {
        return dataLocal;
    }

    public void setDataLocal(String dataLocal) {
        this.dataLocal = dataLocal;
    }

    public String getDataUTC() {
        return dataUTC;
    }

    public void setDataUTC(String dataUTC) {
        this.dataUTC = dataUTC;
    }

    public String getEstacion() {
        return estacion;
    }

    public void setEstacion(String estacion) {
        this.estacion = estacion;
    }

    public Integer getIdEstacion() {
        return idEstacion;
    }

    public void setIdEstacion(Integer idEstacion) {
        this.idEstacion = idEstacion;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Integer getLnIconoCeo() {
        return lnIconoCeo;
    }

    public void setLnIconoCeo(Integer lnIconoCeo) {
        this.lnIconoCeo = lnIconoCeo;
    }

    public Integer getLnIconoTemperatura() {
        return lnIconoTemperatura;
    }

    public void setLnIconoTemperatura(Integer lnIconoTemperatura) {
        this.lnIconoTemperatura = lnIconoTemperatura;
    }

    public Integer getLnIconoVento() {
        return lnIconoVento;
    }

    public void setLnIconoVento(Integer lnIconoVento) {
        this.lnIconoVento = lnIconoVento;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getUtmx() {
        return utmx;
    }

    public void setUtmx(String utmx) {
        this.utmx = utmx;
    }

    public String getUtmy() {
        return utmy;
    }

    public void setUtmy(String utmy) {
        this.utmy = utmy;
    }

    public Double getValorSensTermica() {
        return valorSensTermica;
    }

    public void setValorSensTermica(Double valorSensTermica) {
        this.valorSensTermica = valorSensTermica;
    }

    public Double getValorTemperatura() {
        return valorTemperatura;
    }

    public void setValorTemperatura(Double valorTemperatura) {
        this.valorTemperatura = valorTemperatura;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
