package com.acbsoftware.tiempo.model.meteogalicia.rss;

import java.util.Date;
import java.util.Map;

public class RssParserItem {

	private String title;
	private String link;
	private String description;
	private String pubDate;
	private String guid;
	private String dcDate;
	private Map<String, String> cPrazoVariables;
	private Map<String, Map<String, Byte>> cPrazoTemperaturas;
	private String cPrazoDataPredicion;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPubDate() {
		return pubDate;
	}
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getDcDate() {
		return dcDate;
	}
	public void setDcDate(String dcDate) {
		this.dcDate = dcDate;
	}
	public Map<String, String> getcPrazoVariables() {
		return cPrazoVariables;
	}
	public void setcPrazoVariables(Map<String, String> cPrazoVariables) {
		this.cPrazoVariables = cPrazoVariables;
	}
	public Map<String, Map<String, Byte>> getcPrazoTemperaturas() {
		return cPrazoTemperaturas;
	}
	public void setcPrazoTemperaturas(Map<String, Map<String, Byte>> cPrazoTemperaturas) {
		this.cPrazoTemperaturas = cPrazoTemperaturas;
	}
	public String getcPrazoDataPredicion() {
		return cPrazoDataPredicion;
	}
	public void setcPrazoDataPredicion(String cPrazoDataPredicion) {
		this.cPrazoDataPredicion = cPrazoDataPredicion;
	}

	
	
}
