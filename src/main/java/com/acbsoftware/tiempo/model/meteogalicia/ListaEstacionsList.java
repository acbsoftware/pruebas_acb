package com.acbsoftware.tiempo.model.meteogalicia;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListaEstacionsList {

    private List<ListaEstacionsEstacion> listEstadoActual = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public List<ListaEstacionsEstacion> getListEstadoActual() {
        return listEstadoActual;
    }

    public void setListEstadoActual(List<ListaEstacionsEstacion> listEstadoActual) {
        this.listEstadoActual = listEstadoActual;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	@Override
	public String toString() {
		return "ListaEstacionsList [listEstadoActual=" + listEstadoActual.size() + ", additionalProperties="
				+ additionalProperties.size() + "]";
	}

    
}
