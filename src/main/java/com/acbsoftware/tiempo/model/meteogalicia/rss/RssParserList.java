package com.acbsoftware.tiempo.model.meteogalicia.rss;

import java.util.List;

public class RssParserList {

	private String title;
	private String link;
	private String description;
	private String pubDate;
	private String dcDate;
	private List<RssParserItem> listItems = null;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPubDate() {
		return pubDate;
	}
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	public String getDcDate() {
		return dcDate;
	}
	public void setDcDate(String dcDate) {
		this.dcDate = dcDate;
	}
	public List<RssParserItem> getListItems() {
		return listItems;
	}
	public void setListItems(List<RssParserItem> listItems) {
		this.listItems = listItems;
	}
	
	
}
