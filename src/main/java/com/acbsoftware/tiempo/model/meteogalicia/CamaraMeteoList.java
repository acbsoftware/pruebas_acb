package com.acbsoftware.tiempo.model.meteogalicia;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CamaraMeteoList {

    private List<CamaraMeteo> listaCamaras = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public List<CamaraMeteo> getListaCamaras() {
        return listaCamaras;
    }

    public void setListaCamaras(List<CamaraMeteo> listaCamaras) {
        this.listaCamaras = listaCamaras;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
